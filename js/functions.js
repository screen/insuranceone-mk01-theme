var passd = true,
    recaptchachecked = false;

function isValidEmailAddress(emailAddress) {
    'use strict';
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function recaptchaCallback() {
    //If we managed to get into this function it means that the user checked the checkbox.
    recaptchachecked = true;
    jQuery('.g-recaptcha').next('small').addClass('d-none');
}

jQuery(document).ready(function ($) {
    "use strict";

    AOS.init();

    jQuery('.benefits-slider').owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });

    jQuery('input[name=nombre]').on('change', function () {
        if (jQuery('input[name=nombre]').val() == '') {
            jQuery('input[name=nombre]').next('small').removeClass('d-none').html('error');
        } else {
            if (jQuery('input[name=nombre]').val().length < 3) {

                jQuery('input[name=nombre]').next('small').removeClass('d-none').html(admin_url.invalid_nombre);
            } else {
                jQuery('input[name=nombre]').next('small').addClass('d-none');
            }
        }
    });


    jQuery('input[name=email]').on('change', function () {
        if (jQuery('input[name=email]').val() == '') {

            jQuery('input[name=email]').next('small').removeClass('d-none').html(admin_url.error_email);
        } else {
            if (!isValidEmailAddress(jQuery('input[name=email]').val())) {

                jQuery('input[name=email]').next('small').removeClass('d-none').html(admin_url.invalid_email);
            } else {
                jQuery('input[name=email]').next('small').addClass('d-none');
            }
        }
    });

    jQuery('input[name=telefono]').on('change', function () {
        if (jQuery('input[name=telefono]').val() == '') {

            jQuery('input[name=telefono]').next('small').removeClass('d-none').html(admin_url.error_phone);
        } else {
            if (jQuery('input[name=telefono]').val().length < 3) {

                jQuery('input[name=telefono]').next('small').removeClass('d-none').html(admin_url.invalid_phone);
            } else {
                jQuery('input[name=telefono]').next('small').addClass('d-none');
            }
        }
    });

    jQuery('select[name=pais]').on('change', function () {
        if (jQuery('select[name=pais] :selected').val() == '') {
            jQuery('select[name=pais]').next('small').removeClass('d-none').html(admin_url.error_pais);
        } else {
            jQuery('select[name=pais]').next('small').addClass('d-none');
        }
    });

    jQuery('textarea[name=mensaje]').on('change', function () {
        if (jQuery('textarea[name=mensaje]').val() == '') {
            jQuery('textarea[name=mensaje]').next('small').removeClass('d-none').html(admin_url.error_mensaje);
        } else {
            jQuery('textarea[name=mensaje]').next('small').addClass('d-none');
        }
    });



    jQuery('form.contact-form-container').on('submit', function (e) {
        "use strict";
        passd = true;
        e.preventDefault();
        if (jQuery('input[name=nombre]').val() == '') {
            passd = false;
            jQuery('input[name=nombre]').next('small').removeClass('d-none').html(admin_url.error_nombre);
        } else {
            if (jQuery('input[name=nombre]').val().length < 3) {
                passd = false;
                jQuery('input[name=nombre]').next('small').removeClass('d-none').html(admin_url.invalid_nombre);
            } else {
                jQuery('input[name=nombre]').next('small').addClass('d-none');
            }
        }

        if (jQuery('input[name=email]').val() == '') {
            passd = false;
            jQuery('input[name=email]').next('small').removeClass('d-none').html(admin_url.error_email);
        } else {
            if (!isValidEmailAddress(jQuery('input[name=email]').val())) {
                passd = false;
                jQuery('input[name=email]').next('small').removeClass('d-none').html(admin_url.invalid_email);
            } else {
                jQuery('input[name=email]').next('small').addClass('d-none');
            }
        }

        if (jQuery('input[name=telefono]').val() == '') {
            passd = false;
            jQuery('input[name=telefono]').next('small').removeClass('d-none').html(admin_url.error_phone);
        } else {
            if (jQuery('input[name=telefono]').val().length < 3) {
                passd = false;
                jQuery('input[name=telefono]').next('small').removeClass('d-none').html(admin_url.invalid_phone);
            } else {
                jQuery('input[name=telefono]').next('small').addClass('d-none');
            }
        }

        if (jQuery('select[name=pais] :selected').val() == '') {
            passd = false;
            jQuery('select[name=pais]').next('small').removeClass('d-none').html(admin_url.error_pais);
        } else {
            jQuery('select[name=pais]').next('small').addClass('d-none');
        }

        if (jQuery('textarea[name=mensaje]').val() == '') {
            passd = false;
            jQuery('textarea[name=mensaje]').next('small').removeClass('d-none').html(admin_url.error_mensaje);
        } else {
            jQuery('textarea[name=mensaje]').next('small').addClass('d-none');
        }

        if (recaptchachecked == false) {
            passd = false;
            jQuery('.g-recaptcha').next('small').removeClass('d-none').html(admin_url.error_google);
        } else {
            jQuery('.g-recaptcha').next('small').addClass('d-none');
        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: admin_url.ajax_url,
                data: {
                    action: 'ajax_send_contact_form',
                    info: jQuery('.contact-form-container').serialize()
                },
                beforeSend: function () {
                    jQuery('.contact-form-loader').append('<div class="lds-dual-ring"><div>');
                },
                success: function (response) {
                    jQuery('.contact-form-loader').html('');
                    if (response == 'true') {
                        jQuery('.contact-form-response').html(admin_url.success_form);
                    } else {
                        jQuery('.contact-form-response').html(admin_url.error_form);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }

    });

}); /* end of as page load scripts */
