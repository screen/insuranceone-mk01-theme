<?php $google_options = get_option('ioa_google_settings'); ?>
<form class="contact-form-container">
    <div class="container">
        <div class="row no-gutters">
            <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input type="text" class="form-control custom-form-control" name="nombre" placeholder="<?php _e('NOMBRE COMPLETO', 'insuranceone'); ?>" />
                <small class="danger custom-danger d-none error-nombre"></small>
            </div>
            <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input type="text" class="form-control custom-form-control" name="email" placeholder="<?php _e('EMAIL', 'insuranceone'); ?>" />
                <small class="danger custom-danger d-none error-email"></small>
            </div>
            <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input type="text" class="form-control custom-form-control" name="telefono" placeholder="<?php _e('TELÉFONO', 'insuranceone'); ?>" />
                <small class="danger custom-danger d-none error-telefono"></small>
            </div>
            <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php get_template_part('templates/countries-html'); ?>
                <small class="danger custom-danger d-none error-pais"></small>
            </div>
            <div class="contact-form-item col-12">
                <textarea name="mensaje" class="form-control custom-form-control" id="mensaje" cols="30" rows="4" placeholder="<?php _e('MENSAJE', 'insuranceone'); ?>"></textarea>
                <small class="danger custom-danger d-none error-mensaje"></small>
            </div>
            <div class="contact-form-item col-12">
                <div class="g-recaptcha" data-sitekey="<?php echo $google_options['google_key']; ?>" data-callback="recaptchaCallback" data-size="small"></div>
                <small class="danger custom-danger d-none error-google"><?=__("You must validate this Captcha", 'insuranceone');?></small>
            </div>


            <div class="contact-form-submit col-12">
                <button type="submit" class="btn btn-md btn-primary btn-submit"><?php _e('Enviar', 'insuranceone'); ?></button>
                <div class="contact-form-loader"></div>
            </div>
            <div class="contact-form-response col-12"></div>
        </div>
    </div>
</form>
