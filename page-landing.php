<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $bg_hero_id = get_post_meta(get_the_ID(), 'ioa_main_bg_id', true); ?>
        <?php $bg_hero = wp_get_attachment_image_src($bg_hero_id, 'full', false); ?>
        <section class="the-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_hero[0]; ?>);">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="hero-limiter col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
                        <div class="row row-hero align-items-end">
                            <div class="hero-content-left col-xl-6 col-lg-7 col-md-12 col-sm-12 col-12">
                                <a class="navbar-brand" href="<?php echo home_url('/'); ?>" title="<?php echo get_bloginfo('name'); ?>" data-aos="fade-in" data-aos-delay="50">
                                    <?php ?> <?php $custom_logo_id = get_theme_mod('custom_logo'); ?>
                                    <?php $image = wp_get_attachment_image_src($custom_logo_id, 'full'); ?>
                                    <?php if (!empty($image)) { ?>
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                                    <?php } ?>
                                </a>
                                <h1 data-aos="fade-in" data-aos-delay="150"><?php echo get_post_meta(get_the_ID(), 'ioa_main_title_text', true); ?></h1>
                                <div class="hero-content-info" data-aos="fade-in" data-aos-delay="150">
                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ioa_main_content_text', true)); ?>
                                </div>

                                <div class="hero-content-app-logos" data-aos="fade-in" data-aos-delay="350">
                                    <a href="https://apps.apple.com/us/app/insurance-one-app/id1502488489" title="<?php _e('Haz click para descargar en AppStore', 'insuranceone'); ?>" target="_blank" class="hero-app-link"><img src="<?php echo get_template_directory_uri(); ?>/images/apple.jpg" alt="<?php _e('AppStore', 'insuranceone'); ?>" class="img-fluid" /></a>
                                    <a href="https://play.google.com/store/apps/details?id=com.screenmediagroup.glh&hl=es_419" title="<?php _e('Haz click para descargar en Google Play', 'insuranceone'); ?>" target="_blank" class="hero-app-link"><img src="<?php echo get_template_directory_uri(); ?>/images/google.jpg" alt="<?php _e('GooglePlay', 'insuranceone'); ?>" class="img-fluid" /></a>
                                </div>
                            </div>
                            <div class="hero-content-right col-xl-6 col-lg-5 col-md-12 col-sm-12 col-12 align-self-end">
                                <?php $image_hero_id = get_post_meta(get_the_ID(), 'ioa_main_content_image_id', true); ?>
                                <?php $image_hero = wp_get_attachment_image_src($image_hero_id, 'full', false); ?>
                                <img src="<?php echo $image_hero[0]; ?>" alt="<?php echo get_post_meta(get_the_ID(), 'ioa_main_title_text', true); ?>" class="img-fluid img-hero" data-aos="fade-left" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-benefits col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="benefits-limiter col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
                        <div class="row align-items-start justify-content-center">
                            <div class="benefits-intro col-xl-6 col-lg-8 col-md-12 col-sm-12 col-12">
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ioa_benefits_content_text', true)); ?>
                            </div>
                            <div class="w-100"></div>
                            <?php $group_benefits = get_post_meta(get_the_ID(), 'ioa_benefits_group', true); ?>
                            <?php $quantity_benefits = count($group_benefits); ?>
                            <?php $process_benefits = $quantity_benefits / 2; ?>
                            <div class="benefits-left col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <?php $i = 1; ?>
                                <?php foreach ($group_benefits as $item_benefits) { ?>
                                <div class="benefits-item" data-aos="fade-in" data-aos-delay="150">
                                    <img src="<?php echo $item_benefits['icon']; ?>" alt="<?php echo $item_benefits['title']; ?>" class="img-fluid" />
                                    <div class="benefits-item-info">
                                        <h2><?php echo $item_benefits['title']; ?></h2>
                                        <?php echo $item_benefits['description']; ?>
                                    </div>
                                </div>
                                <?php if ($i == $process_benefits) {
                                        break;
                                    } ?>
                                <?php $i++;
                                } ?>
                            </div>
                            <div class="benefits-left col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 d-xl-none d-lg-none d-md-block d-sm-block d-block">
                                <?php $i = 1; ?>
                                <?php foreach ($group_benefits as $item_benefits) { ?>
                                <?php if ($i > $process_benefits) { ?>
                                <div class="benefits-item" data-aos="fade-in" data-aos-delay="150">
                                    <img src="<?php echo $item_benefits['icon']; ?>" alt="<?php echo $item_benefits['title']; ?>" class="img-fluid" />
                                    <div class="benefits-item-info">
                                        <h2><?php echo $item_benefits['title']; ?></h2>
                                        <?php echo $item_benefits['description']; ?>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php $i++;
                                } ?>
                            </div>

                            <div class="benefits-center col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="150">
                                <?php $slider_benefits = get_post_meta(get_the_ID(), 'ioa_benefits_phone_slider', true); ?>
                                <div class="benefits-slider owl-carousel owl-theme">
                                    <?php foreach ($slider_benefits as $slider_item) { ?>
                                    <div class="item">
                                        <img src="<?php echo $slider_item; ?>" alt="Slider Image" class="img-fluid img-slider-app" />
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="benefits-right col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 d-xl-block d-lg-block d-md-none d-sm-none d-none">
                                <?php $i = 1; ?>
                                <?php foreach ($group_benefits as $item_benefits) { ?>
                                <?php if ($i > $process_benefits) { ?>
                                <div class="benefits-item" data-aos="fade-in" data-aos-delay="150">
                                    <img src="<?php echo $item_benefits['icon']; ?>" alt="<?php echo $item_benefits['title']; ?>" class="img-fluid" />
                                    <div class="benefits-item-info">
                                        <h2><?php echo $item_benefits['title']; ?></h2>
                                        <?php echo $item_benefits['description']; ?>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php $i++;
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-contact col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="contact-limiter col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
                        <div class="row align-items-start justify-content-center">
                            <div class="contact-left col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="150">
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ioa_contact_form_text', true)); ?>
                                <div class="contact-email-container">
                                    <i class="fa fa-envelope-o"></i><a href="mailto:info@insuranceoneapp.com">info@insuranceoneapp.com</a>
                                </div>
                            </div>
                            <div class="contact-right col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12" data-aos="fade-in" data-aos-delay="350">
                                <div class="contact-form-pre-title">
                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ioa_contact_form_superior_text', true)); ?>
                                </div>
                                <?php get_template_part('templates/contact-form'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>