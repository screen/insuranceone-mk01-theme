<?php
function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}

add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );


function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

add_action( 'cmb2_admin_init', 'insuranceone_register_custom_metabox' );
function insuranceone_register_custom_metabox() {
    $prefix = 'ioa_';

    /* --------------------------------------------------------------
    1.- LANDING: HERO SECTION
    -------------------------------------------------------------- */
    $cmb_landing_hero = new_cmb2_box( array(
        'id'            => $prefix . 'main_title',
        'title'         => esc_html__( 'Landing: Hero Principal', 'insuranceone' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );


    $cmb_landing_hero->add_field( array(
        'id'      => $prefix . 'main_bg',
        'name'      => esc_html__( 'Imagen de Fondo', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese un fondo apropiado para el Hero Principal', 'insuranceone' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Fondo del Hero', 'insuranceone' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_landing_hero->add_field( array(
        'id'      => $prefix . 'main_title_text',
        'name'      => esc_html__( 'Título principal', 'insuranceone' ),
        'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'insuranceone' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_landing_hero->add_field( array(
        'id'      => $prefix . 'main_content_text',
        'name'      => esc_html__( 'Texto principal', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese aquí el texto del Hero principal', 'insuranceone' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );


    $cmb_landing_hero->add_field( array(
        'id'      => $prefix . 'main_content_image',
        'name'      => esc_html__( 'Imagen del Hero', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese una Imagen Descriptiva para el Hero Principal', 'insuranceone' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'insuranceone' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    /* --------------------------------------------------------------
    1.- LANDING: BENEITS SECTION
    -------------------------------------------------------------- */
    $cmb_landing_benefits = new_cmb2_box( array(
        'id'            => $prefix . 'landing_benefits',
        'title'         => esc_html__( 'Landing: Beneficios', 'insuranceone' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );    

    $cmb_landing_benefits->add_field( array(
        'id'      => $prefix . 'benefits_content_text',
        'name'      => esc_html__( 'Texto principal', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese aquí el texto Introductorio de los Beneficios', 'insuranceone' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_landing_benefits->add_field( array(
        'id'      => $prefix . 'benefits_phone_slider',
        'name'      => esc_html__( 'Imágenes del APP', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese aquí las imágenes del APP que estaran dentro del slider', 'insuranceone' ),
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
        'query_args' => array( 'type' => 'image' ), // Only images attachment
        'text' => array(
            'add_upload_files_text' => esc_html__( 'Agregar Slides', 'insuranceone' ), 
            'remove_image_text' => esc_html__( 'Remover Slide', 'insuranceone' ), 
            'file_text' => esc_html__( 'Slide', 'insuranceone' ), 
            'file_download_text' => esc_html__( 'Descargar', 'insuranceone' ), 
            'remove_text' => esc_html__( 'Remover', 'insuranceone' ), 
        ),
    ) );

    $group_field_id = $cmb_landing_benefits->add_field( array(
        'id'          => $prefix . 'benefits_group',
        'type'        => 'group',
        'description' => __( 'Grupo de Beneficios dentro de la sección', 'insuranceone' ),
        'options'     => array(
            'group_title'       => __( 'Item {#}', 'insuranceone' ),
            'add_button'        => __( 'Agregar otro Item', 'insuranceone' ),
            'remove_button'     => __( 'Remover Item', 'insuranceone' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( '¿Esta seguro de eliminar este Item?', 'insuranceone' )
        )
    ) );

    $cmb_landing_benefits->add_group_field( $group_field_id, array(
        'id'   => 'icon',
        'name'      => esc_html__( 'Imagen Icono del Beneficio', 'insuranceone' ),
        'desc'      => esc_html__( 'Cargue un icono descriptivo del Beneficio', 'insuranceone' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Icono', 'insuranceone' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_landing_benefits->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Título del Beneicio', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese un titulo descriptivo del Beneficio', 'insuranceone' ),
        'type' => 'text'
    ) );

    $cmb_landing_benefits->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Texto del Beneficio', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese un texto descriptivo del Beneficio', 'insuranceone' ),
        'type' => 'textarea_small'
    ) );

    /* --------------------------------------------------------------
    3.- LANDING: CONTACT FORM SECTION
    -------------------------------------------------------------- */
    $cmb_landing_contact = new_cmb2_box( array(
        'id'            => $prefix . 'contact_metabox',
        'title'         => esc_html__( 'Landing: Formulario de Contacto', 'insuranceone' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_landing_contact->add_field( array(
        'id'      => $prefix . 'contact_form_text',
        'name'      => esc_html__( 'Texto de la Sección', 'insuranceone' ),
        'desc'      => esc_html__( 'Coloque en Negrillas el texto que tendra el color diferente', 'insuranceone' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_landing_contact->add_field( array(
        'id'      => $prefix . 'contact_form_superior_text',
        'name'      => esc_html__( 'Texto Superior', 'insuranceone' ),
        'desc'      => esc_html__( 'Ingrese el texto que estará ubicado en la parte superior del formulario', 'insuranceone' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );
}
