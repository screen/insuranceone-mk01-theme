<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action( 'customize_register', 'insuranceapp_customize_register' );

function insuranceapp_customize_register( $wp_customize ) {

    /* SOCIAL */
    $wp_customize->add_section('ioa_social_settings', array(
        'title'    => __('Redes Sociales', 'insuranceapp'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'insuranceapp'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('ioa_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'insuranceapp_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'ioa_social_settings',
        'settings' => 'ioa_social_settings[facebook]',
        'label' => __( 'Facebook', 'insuranceapp' ),
    ) );

    $wp_customize->add_setting('ioa_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'insuranceapp_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'ioa_social_settings',
        'settings' => 'ioa_social_settings[twitter]',
        'label' => __( 'Twitter', 'insuranceapp' ),
    ) );

    $wp_customize->add_setting('ioa_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'insuranceapp_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'ioa_social_settings',
        'settings' => 'ioa_social_settings[instagram]',
        'label' => __( 'Instagram', 'insuranceapp' ),
    ) );

    $wp_customize->add_setting('ioa_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'insuranceapp_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'ioa_social_settings',
        'settings' => 'ioa_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'insuranceapp' ),
    ) );

    $wp_customize->add_setting('ioa_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'insuranceapp_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'ioa_social_settings',
        'settings' => 'ioa_social_settings[youtube]',
        'label' => __( 'YouTube', 'insuranceapp' ),
    ) );

    $wp_customize->add_setting('ioa_social_settings[yelp]', array(
        'default'           => '',
        'sanitize_callback' => 'insuranceapp_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'yelp', array(
        'type' => 'url',
        'section' => 'ioa_social_settings',
        'settings' => 'ioa_social_settings[yelp]',
        'label' => __( 'Yelp', 'insuranceapp' ),
    ) );


    $wp_customize->add_section('ioa_cookie_settings', array(
        'title'    => __('Cookies', 'insuranceapp'),
        'description' => __('Opciones de Cookies', 'insuranceapp'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('ioa_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'insuranceapp'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'ioa_cookie_settings',
        'settings' => 'ioa_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('ioa_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'ioa_cookie_settings',
        'settings' => 'ioa_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'insuranceapp' ),
    ) );


    $wp_customize->add_section('ioa_google_settings', array(
        'title'    => __('Google Recaptcha', 'insuranceapp'),
        'description' => __('Opciones de Google Recaptcha', 'insuranceapp'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('ioa_google_settings[google_key]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'google_key', array(
        'type' => 'text',
        'label'    => __('Google Key', 'insuranceapp'),
        'description' => __( 'Ingrese la Key de Google Recaptcha.' ),
        'section'  => 'ioa_google_settings',
        'settings' => 'ioa_google_settings[google_key]'
    ));

    $wp_customize->add_setting('ioa_google_settings[google_secret]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'google_secret', array(
        'type' => 'text',
        'label'    => __('Google Secret', 'insuranceapp'),
        'description' => __( 'Ingrese la Secret de Google Recaptcha.' ),
        'section'  => 'ioa_google_settings',
        'settings' => 'ioa_google_settings[google_secret]'
    ));
}

function insuranceapp_sanitize_url( $url ) {
    return esc_url_raw( $url );
}
