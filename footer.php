<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="footer-limiter col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
                        <div class="row align-items-center justify-content-center">
                            <div class="footer-left col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <h5><?php printf( __('Powered by <a href="%s">GLH Global</a>', 'insuranceone'), 'http://screen.com.ve'); ?></h5>
                            </div>
                            <div class="footer-right col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <h5>Copyright &copy; 2019 - <?php printf( __('Desarrollado por <a href="%s">SMG | Digital Marketing Agency</a>', 'insuranceone'), 'http://screen.com.ve'); ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>
